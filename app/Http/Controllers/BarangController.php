<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Barang; 

class BarangController extends Controller
{
    //

    public function store (Request $request) { 
        $request->validate([
            'kode_barang'  => 'required' , 
            'nama_barang' => 'required' , 
            'gambar_barang' => 'required|image|mimes:jpeg,png,jpg',
        ]); 


    }

    public function index() { 
        $barang = Barang::paginate('20'); 
        // return view
    }

    
}
